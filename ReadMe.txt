===================================
DupeFinder.py
===================================

DupeFinder.py is a python script which will find duplicate files
within a directory tree and present them to the user to delete or
keep. Regardless of how many files share the same checksum, they
are presented in pairs so that the user is always presented with
only three options (keep 1, keep 2, keep both). The files are
presented in a way that ensures any combination of keep/delete 
decisions is possible for a set of duplicates. The script will 
search from within the current working directory, so place it in
the folder you wish to search.

NOTE!! - At this point there is a serious bug. DupeFinder.py finds
many false duplicates among the true ones. I therefore implemented
DupeFinder_Graphic.py to visually confirm that pictures are 
duplicates. The Sample Data folder includes both true duplicates
and different files which DupeFinder marks as duplicates.

===================================
DupeFinder_Graphic.py
===================================

DupeFinder_Graphic.py takes all the code from DupeFinder.py and
adds the feature of displaying the duplicate images side by side
so the user can verify that they are duplicates. The user input
is still done through a command line interface. It requires the
cv2 and numpy modules to do this. cv2 is included since it is 
easy to include and not as commonly installed. Numpy is a more
substantial and more common module so it is left to the user to
ensure that it is installed on their system. 