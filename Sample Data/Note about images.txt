These images are all in the public domain, and were accessed from
http://www.public-domain-image.com/

I chose pictures more-or-less at random, and it didn't take long to
find pairs of dissimilar pictures that DupeFinder.py marked as 
duplicates. 