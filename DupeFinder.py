# DupeFinder.py
# Author: Matthew Rowley
# Last Modified: Sept. 19, 2014
#
# Licensed under a 3-Clause BSD License
# Full license text is at the end of this file

''' This program will identify duplicate files and present
    them to the user to decide which (if any) to delete.
'''

import os
import sys
import hashlib

def main():
    'The main procedure'
    folder_path = os.getcwd()
    dupe_dict = findDupes(folder_path)
    print str(len(dupe_dict)) + ' repeated files found\n'
    print 'WARNING: Deletion is final. Files are NOT moved to the "Recycle Bin".'
    deleteDupes(dupe_dict, folder_path)
    print '\nFile deletion complete.'
    print '\n1] Search for and remove empty folders'
    print 'else] Exit now'
    user_selection = raw_input('Make a selection [1,else]: ')
    if user_selection != '1':
        sys.exit()
    deleteFolders()
    user_selection = raw_input('\nProgram complete. Press "Enter" to close interpreter.')
    sys.exit()
# End of main():

def findDupes(folder_path):
    'Calculate sha256 hashes of all files and compare them to find duplicates'
    hash_dict = {}
    dupe_dict = {}
    iterator = os.walk(folder_path)
    print 'Calculating sha256 hashes and compiling a list of duplicates.'
    print 'This may take some time.'
    for root, dirs, files in iterator:
        for filename in files:
            filepath = os.path.join(root, filename)
            filehash = hashlib.sha256(open(filepath).read()).hexdigest()
            if filehash in hash_dict:  # This hash has been encountered previously
                if filehash in dupe_dict:  # There are already dupes with this hash. Add another!
                    dupe_dict[filehash].append(filepath)
                else:  # These are the first dupes with this hash
                    dupe_dict[filehash] = [hash_dict[filehash], filepath]
            else:  # First time this hash has been encountered
                hash_dict[filehash] = filepath
    return dupe_dict
#End of findDupes()

def deleteDupes(dupe_dict, folder_path):
    'Present sets of duplicate files to the user to decide which ones to keep'
    i = 0
    for dupe_sum in dupe_dict:
        remaining = len(dupe_dict) - i
        while (len(dupe_dict[dupe_sum]) > 1):
            print "\n" + str(remaining) + ' repeated hashes remaining'
            print str(len(dupe_dict[dupe_sum])) + ' files with current hash'
            path1 = dupe_dict[dupe_sum][0]
            path2 = dupe_dict[dupe_sum][1]
            print '\n1] Keep ' + path1.replace(folder_path, '')[1:]
            print '2] Keep ' + path2.replace(folder_path, '')[1:]
            print 'B] Keep Both'
            user_selection = raw_input('Make a selection [1,2,B]: ')
            if user_selection == '1':
                dupe_dict[dupe_sum].remove(path2)
                print 'Deleting: ' + path2
                os.remove(path2)
            elif user_selection == '2':
                dupe_dict[dupe_sum].remove(path1)
                print 'Deleting: ' + path1
                os.remove(path1)
            elif user_selection == 'B':
                dupe_dict[dupe_sum].remove(path2)
            else:
                print 'INVALID SELECTION'
        i = i + 1
# End of deleteDupes(dupe_dict)

def deleteFolders():
    deleted = True
    while (deleted is True):  # This is ugly. It deletes nested empty folders of arbitrary depth
        deleted = False
        iterator = os.walk(folder_path)
        for root, dirs, files in iterator:
            for directory in dirs:
                try:
                    os.rmdir(os.path.join(root, directory))  # non-empty directories throw an exception
                    deleted = True
                except OSError:
                    pass
# End of deleteFolders()

# Here is the code
if __name__ == '__main__':
    main()

# By downloading, copying, installing or using the software you agree to this license.
# If you do not agree to this license, do not download, install,
# copy or use the software.
# 
# 
#                           License Agreement
#             For DupeFinder and DupeFinder_Graphic Scripts
#                        (3-clause BSD License)
# 
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
# 
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions and the following disclaimer.
# 
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
# 
#   * Neither the names of the copyright holders nor the names of the contributors
#     may be used to endorse or promote products derived from this software
#     without specific prior written permission.
# 
# This software is provided by the copyright holders and contributors "as is" and
# any express or implied warranties, including, but not limited to, the implied
# warranties of merchantability and fitness for a particular purpose are disclaimed.
# In no event shall copyright holders or contributors be liable for any direct,
# indirect, incidental, special, exemplary, or consequential damages
# (including, but not limited to, procurement of substitute goods or services;
# loss of use, data, or profits; or business interruption) however caused
# and on any theory of liability, whether in contract, strict liability,
# or tort (including negligence or otherwise) arising in any way out of
# the use of this software, even if advised of the possibility of such damage.
